# Elasticsearch安装使用及SpringBoot集成ES

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;笔者为了操作方便，这里直接选用传统的解压操作的方式安装，不用Docker来进行安装。

## 一、elasticsearch安装

进入官网：https://www.elastic.co
选择历史版本，笔者这里选择的是6.6.2版本

```
#解压tar包
tar -zxvf elasticsearch-6.6.2.tar.gz
# 启动单实例ElasticSearch
cd elasticsearch-6.6.2/bin
sh elasticsearch
```

访问http://localhost:9200 
可以看到如下，说明ElasticSearch已经启动成功。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/112440_59c9ba85_880818.jpeg "15585367526213.jpg")

elasticsearch默认的集群名称cluster_name为elasticsearch，默认端口为9200。

## 二、安装elasticsearch-head

elasticSearch-head是一款专门针对于elasticsearch的客户端工具，是一个基于node.js的前端工程。

node.js的安装方法： http://www.runoob.com/nodejs/nodejs-install-setup.html 笔者的版本是V6.3.0。

elasticsearch-head的github官方地址：https://github.com/mobz/elasticsearch-head

```
git clone git://github.com/mobz/elasticsearch-head.git
cd elasticsearch-head
npm install
npm run start
```

访问：http://localhost:9100
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/112713_db91cd5d_880818.jpeg "15585387115832.jpg")

此时发现集群健康值是灰的，这是由于elasticSearch和elasticSearch-head本身是两个独立的进程的，因此他们之间的访问是有跨域问题的。因此我们需要修改elasticSearch的配置文件。

打开elasticsearch中config文件夹下的elasticsearch.yml，在末尾加上：

```
# 跨域
http.cors.enabled: true
http.cors.allow-origin: "*"
```
重启elasticSearch后，发现集群健康值是绿色的。说明elasticsearch-head已经成功连接上了elasticsearch。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/112801_d0c0f27d_880818.jpeg "15585388506984.jpg")


## 三、elasticSearch的横向扩展

笔者这里设置一个master节点，两个salve节点。

首先我们来设置master节点，设置集群名称是：calvin

```
# 设置master
cluster.name: calvin
node.name: master
node.master: true

# 绑定的ip
network.host: 127.0.0.1
```

重启elasticsearch，可以看到
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/113317_14e6d423_880818.jpeg "15585392045650.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/113403_29a4e5f4_880818.jpeg "15585392212388.jpg")


接着我们复制一份解压包，创建salve1节点。修改elasticsearch.yml。注意设置的集群名称要与master节点保持一致。

```
# 设置salve
cluster.name: calvin
node.name: salve1

# 绑定的ip
network.host: 127.0.0.1

# 设置端口，防止与master的端口重复
http.port: 8200

# 设置集群单播，连接master节点
discovery.zen.ping.unicast.hosts: ["127.0.0.1"]
```

salve2节点的设置类似salve1，将node.name改为salve2，port改为7200，具体配置如下：

```
# 设置salve
cluster.name: calvin
node.name: salve2

# 绑定的ip
network.host: 127.0.0.1

# 设置端口，防止与master的端口重复
http.port: 7200

# 设置集群单播，连接master节点
discovery.zen.ping.unicast.hosts: ["127.0.0.1"]
```

分别启动salve1和salve2节点，刷新elasticsearch-head，可以看到，master以及两个salve节点都已经展示了。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/113740_4f61cc23_880818.jpeg "15585402346639.jpg")


## 四、elasticSearch的基本概念与基本操作

```
索引：含有相同属性的文档集合
类型：索引可以定义一个或多个类型，文档必须属于一个类型
文档：文档是可以被索引的基本数据单位
分片：每个索引都有多个分片，每个分片是一个Lucene索引
备份：拷贝一份分片就完成了分片的备份
```

类比数据库来看，索引相当于是数据库，索引就是表，而文档就是表中的一条条记录。换个更恰当的比喻，比如说有一个信息查询系统，汽车索引，图书索引，家居索引。比如说，图书索引可以分为科普类型，小说类型等，每个类型有多本书，具体到每一本书就是文档。

API基本格式：http://<ip>:<port>/<索引></类型></文档id>
常用HTTP动词：GET/PUT/POST/DELETE 

#### （1）创建索引

* 非结构化创建
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/114024_4eb04233_880818.jpeg "15586142780923.jpg")

索引-->新建索引，默认分片数是5，备份数是1。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/114041_fcc93740_880818.jpeg "15586143555329.jpg")

其中黑粗框表示主分片，对应的黑细框表示主分片对应的备份。

点击book下的信息-->索引信息，可以看到mappings是空的，表示该索引为非结构的索引。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/114133_098ee19e_880818.jpeg "15586145091344.jpg")

* 结构化创建
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/114211_d25bc51d_880818.jpeg "15586161176392.jpg")

请求方式改为PUT，请求数据为：

```
{
    "settings":{
        "number_of_shards":3,
        "number_of_replicas":1
    },
    "mappings":{
        "man":{
            "properties":{
                "name":{
                    "type":"text"
                },
                "country":{
                    "type":"keyword"
                },
                "age":{
                    "type":"integer"
                },
                "date":{
                    "type":"date",
                    "format":"yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
                }
            }
        }
    }
}
```
表示建立people索引，有3个分片，1个备份，类型为man，类型属性包含name,country,age,date。

通过elasticsearch-head中点击信息-->索引信息，可以看到：
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/115513_9af0fbd8_880818.jpeg "15586162566402.jpg")

注意一个点，6.0以上的的版本不允许一个index下面有多个type，并且官方说是在接下来的7.0版本中会删掉type。

#### （2）插入

* 指定文档id插入
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/115617_0fe2d6d5_880818.jpeg "15586168652587.jpg")

请求方式改为PUT，请求数据为：

```
{
    "name":"calvin",
    "country":"China",
    "age":27,
    "date":"1993-12-20"
}
```

刷新elasticsearch-head，可以看到docs数量为1了。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/115648_68ad6422_880818.jpeg "15586169527481.jpg")

点击数据浏览，可以看到我们刚才插入的数据了。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/115706_68b19a5f_880818.jpeg "15586170136479.jpg")

* 自动产生文档id插入
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/115732_d3585673_880818.jpeg "15586172392479.jpg")

请求方式改为POST，请求数据为：

```
{
    "name":"sheng",
    "country":"China",
    "age":30,
    "date":"1990-05-24"
}
```

接着刷新elasticsearch-head，可以看到，已经自动创建了一条doc。
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/120048_38bb5f8b_880818.jpeg "15586173182129.jpg")


#### （3）修改

修改我们的文档信息，比如说修改文档id=1中name属性的值
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/120120_5395cad7_880818.jpeg "15586176292879.jpg")

请求方式改为POST，请求数据为：

```
{
    "doc":{
        "name":"周杰伦"
    }
}
```

刷新elasticsearch-head，可以看到文档id=1的记录中，name已经从calvin改为了周杰伦。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/141057_5efb5399_880818.jpeg "15586177882096.jpg")

#### （4）删除

比如我们要删除文档id=1的记录

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/141219_9dc89b55_880818.jpeg "15586181587864.jpg")

请求方式为DELETE。刷新elasticsearch-head，可以看到文档id=1的记录已经被删除。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/141244_f1d0bfc8_880818.jpeg "15586182220012.jpg")

同理比如我们要删除类型，则请求url改为：http://localhost:9200/people/man/ 删除索引则改为：http://localhost:9200/people/

当然，我们也可以在elasticsearch-head中直接删除索引，例如我们删除之前创建的book索引。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/141309_6140b19d_880818.jpeg "15586185334397.jpg")

可以看到，book索引已经被成功删除。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/141337_24a23ee1_880818.jpeg "15586185659653.jpg")

#### （5）查询

为了做测试，笔者这里新建了几条数据

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/141703_fb6ef83c_880818.jpeg "15586196565163.jpg")

* 简单查询

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/141853_9772674a_880818.jpeg "15586188772725.jpg")


* 条件查询

如果是查询people索引下所有的数据。请求url：http://localhost:9200/people/_search

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/145110_68871ea0_880818.jpeg "15586197159082.jpg")


请求方式改为POST，请求数据为：

```
{
	"query":{
		"match_all":{}
	}
}
```

指定返回的数量以及从哪里返回。我们修改请求字段：

```
{
	"query":{
		"match_all":{}
	},
	"from": 1,
	"size": 1
}
```

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/145147_4e47ffa2_880818.jpeg "15586197560644.jpg")


筛选年龄为25，并按照date（出生年月日）降序返回。

请求方式改为POST，请求数据为：

```
{
    "query":{
        "match":{
            "age":25
        }
    },
    "sort":[
        {
            "date":{
                "order":"desc"
            }
        }
    ]
}
```

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/145225_8074c441_880818.jpeg "15586201694685.jpg")

返回数据为：

```
{
    "took": 39,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 3,
        "max_score": null,
        "hits": [
            {
                "_index": "people",
                "_type": "man",
                "_id": "3",
                "_score": null,
                "_source": {
                    "name": "李四",
                    "country": "China",
                    "age": 25,
                    "date": "1995-10-17"
                },
                "sort": [
                    813888000000
                ]
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "4",
                "_score": null,
                "_source": {
                    "name": "王五",
                    "country": "China",
                    "age": 25,
                    "date": "1995-09-18"
                },
                "sort": [
                    811382400000
                ]
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "5",
                "_score": null,
                "_source": {
                    "name": "王六",
                    "country": "China",
                    "age": 25,
                    "date": "1995-06-18"
                },
                "sort": [
                    803433600000
                ]
            }
        ]
    }
}
```

* 聚合查询

首先我们来查询聚合age属性，请求数据是：

```
{
    "aggs":{
        "group_by_age":{
            "terms":{
                "field":"age"
            }
        }
    }
}
```

其中的group_by_age是可以自定义的。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/145348_42532691_880818.jpeg "15586210024840.jpg")

返回数据是：

```
{
    "took": 8,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 5,
        "max_score": 1,
        "hits": [
            {
                "_index": "people",
                "_type": "man",
                "_id": "iIHS5GoBkW5tmvoc_T_u",
                "_score": 1,
                "_source": {
                    "name": "sheng",
                    "country": "China",
                    "age": 30,
                    "date": "1990-05-24"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "2",
                "_score": 1,
                "_source": {
                    "name": "张三",
                    "country": "China",
                    "age": 28,
                    "date": "1992-11-20"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "4",
                "_score": 1,
                "_source": {
                    "name": "王五",
                    "country": "China",
                    "age": 25,
                    "date": "1995-09-18"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "5",
                "_score": 1,
                "_source": {
                    "name": "王六",
                    "country": "China",
                    "age": 25,
                    "date": "1995-06-18"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "3",
                "_score": 1,
                "_source": {
                    "name": "李四",
                    "country": "China",
                    "age": 25,
                    "date": "1995-10-17"
                }
            }
        ]
    },
    "aggregations": {
        "group_by_age": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
                {
                    "key": 25,
                    "doc_count": 3
                },
                {
                    "key": 28,
                    "doc_count": 1
                },
                {
                    "key": 30,
                    "doc_count": 1
                }
            ]
        }
    }
}
```

可以看到根据age来聚合的数据分析来看，25岁的3个，28岁和30岁的各1个。

接着我们可以对age字段做分析，使用stats方法。请求数据为：

```
{
    "aggs":{
        "group_by_age":{
            "stats":{
                "field":"age"
            }
        }
    }
}
```

返回数据是：

```
{
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 5,
        "max_score": 1,
        "hits": [
            {
                "_index": "people",
                "_type": "man",
                "_id": "iIHS5GoBkW5tmvoc_T_u",
                "_score": 1,
                "_source": {
                    "name": "sheng",
                    "country": "China",
                    "age": 30,
                    "date": "1990-05-24"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "2",
                "_score": 1,
                "_source": {
                    "name": "张三",
                    "country": "China",
                    "age": 28,
                    "date": "1992-11-20"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "4",
                "_score": 1,
                "_source": {
                    "name": "王五",
                    "country": "China",
                    "age": 25,
                    "date": "1995-09-18"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "5",
                "_score": 1,
                "_source": {
                    "name": "王六",
                    "country": "China",
                    "age": 25,
                    "date": "1995-06-18"
                }
            },
            {
                "_index": "people",
                "_type": "man",
                "_id": "3",
                "_score": 1,
                "_source": {
                    "name": "李四",
                    "country": "China",
                    "age": 25,
                    "date": "1995-10-17"
                }
            }
        ]
    },
    "aggregations": {
        "group_by_age": {
            "count": 5,
            "min": 25,
            "max": 30,
            "avg": 26.6,
            "sum": 133
        }
    }
}
```

可以看到最后对age的分析，包括总个数，最大值，最小值，平均数以及求和。

#### （6）高级查询

在做高级查询之前，我们先要提前准备一些数据。创建book索引，类型为novel，创建10个doc。

具体的建立索引及文档参考上文中people。由于篇幅有限，笔者把脚本放在码云上，地址：https://gitee.com/calvin-sheng/Technical-Document/blob/master/创建book索引及Doc文档脚本.txt  读者可以自行复制粘贴到postman运行创建。

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155102_a7a91428_880818.jpeg "15586275989843.jpg")


###### 1、子条件查询：表示特定字段查询所指特定值。

* Query Context

在查询过程中，除了判断文档是否满足查询条件外，ES还会计算一个_SCORE来标识匹配的程度，旨在判断目标文档和查询条件匹配的有多好。

1.全文本查询：针对文本类型数据
  
```
（1） 使用match关键字模糊匹配

{
    "query":{
        "match":{
            "title":"入门"
        }
    }
}

返回数据

{
    "took": 7,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 3,
        "max_score": 2.4861822,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "3",
                "_score": 2.4861822,
                "_source": {
                    "author": "李四",
                    "title": "Python入门",
                    "word_count": 2000,
                    "publish_date": "2005-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "2",
                "_score": 1.7968802,
                "_source": {
                    "author": "张三",
                    "title": "Java入门",
                    "word_count": 2000,
                    "publish_date": "2010-10-02"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "4",
                "_score": 1.7968802,
                "_source": {
                    "author": "王五",
                    "title": "ElasticSearch入门",
                    "word_count": 1000,
                    "publish_date": "2010-10-01"
                }
            }
        ]
    }
}

（2）使用match_phrase关键字习语匹配

{
    "query":{
        "match_phrase":{
            "title":"ElasticSearch"
        }
    }
}

返回数据

{
    "took": 2,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 1.4226693,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "4",
                "_score": 1.4226693,
                "_source": {
                    "author": "王五",
                    "title": "ElasticSearch入门",
                    "word_count": 1000,
                    "publish_date": "2010-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "10",
                "_score": 1.2430911,
                "_source": {
                    "author": "周文",
                    "title": "ElasticSearch实战",
                    "word_count": 10000,
                    "publish_date": "2015-09-28"
                }
            }
        ]
    }
}

（3）使用multi_match查询作者和标题包含calvin的数据

{
    "query":{
        "multi_match":{
            "query":"calvin",
            "fields":["author","title"]
        }
    }
}

返回数据

{
    "took": 11,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 1.4599355,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "1",
                "_score": 1.4599355,
                "_source": {
                    "author": "calvin",
                    "title": "太极拳",
                    "word_count": 1000,
                    "publish_date": "2000-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "9",
                "_score": 0.96669346,
                "_source": {
                    "author": "王尊",
                    "title": "calvin的绝命传说",
                    "word_count": 6000,
                    "publish_date": "2008-10-09"
                }
            }
        ]
    }
}

（4）使用query_string进行语法查询

{
    "query":{
        "query_string":{
            "query":"(ElasticSearch AND 实战) OR Python"
        }
    }
}

返回数据

{
    "took": 5,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 3.9337728,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "10",
                "_score": 3.9337728,
                "_source": {
                    "author": "周文",
                    "title": "ElasticSearch实战",
                    "word_count": 10000,
                    "publish_date": "2015-09-28"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "3",
                "_score": 1.3112576,
                "_source": {
                    "author": "李四",
                    "title": "Python入门",
                    "word_count": 2000,
                    "publish_date": "2005-10-01"
                }
            }
        ]
    }
}

（5）使用query_string查询多个字段

{
    "query":{
        "query_string":{
        	"fields":["title","author"],
            "query":"ElasticSearch OR calvin"
        }
    }
}

返回数据

{
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 4,
        "max_score": 1.4599355,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "1",
                "_score": 1.4599355,
                "_source": {
                    "author": "calvin",
                    "title": "太极拳",
                    "word_count": 1000,
                    "publish_date": "2000-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "4",
                "_score": 1.4226693,
                "_source": {
                    "author": "王五",
                    "title": "ElasticSearch入门",
                    "word_count": 1000,
                    "publish_date": "2010-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "10",
                "_score": 1.3112576,
                "_source": {
                    "author": "周文",
                    "title": "ElasticSearch实战",
                    "word_count": 10000,
                    "publish_date": "2015-09-28"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "9",
                "_score": 0.96669346,
                "_source": {
                    "author": "王尊",
                    "title": "calvin的绝命传说",
                    "word_count": 6000,
                    "publish_date": "2008-10-09"
                }
            }
        ]
    }
}

```

2.字段级别查询：针对结构化数据，如数字、日期等

```
（1）查询字数在某个特定集（1000）的书籍

{
    "query":{
        "term":{
            "word_count":1000
        }
    }
}

返回数据

{
    "took": 14,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 1,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "4",
                "_score": 1,
                "_source": {
                    "author": "王五",
                    "title": "ElasticSearch入门",
                    "word_count": 1000,
                    "publish_date": "2010-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "1",
                "_score": 1,
                "_source": {
                    "author": "calvin",
                    "title": "太极拳",
                    "word_count": 1000,
                    "publish_date": "2000-10-01"
                }
            }
        ]
    }
}

（2）查询字符在某个范围（大于等于1000-小于等于2000）的书籍

{
    "query":{
        "range":{
            "word_count":{
                "gte":1000,
                "lte":2000
            }
        }
    }
}

返回数据

{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 4,
        "max_score": 1,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "2",
                "_score": 1,
                "_source": {
                    "author": "张三",
                    "title": "Java入门",
                    "word_count": 2000,
                    "publish_date": "2010-10-02"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "4",
                "_score": 1,
                "_source": {
                    "author": "王五",
                    "title": "ElasticSearch入门",
                    "word_count": 1000,
                    "publish_date": "2010-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "3",
                "_score": 1,
                "_source": {
                    "author": "李四",
                    "title": "Python入门",
                    "word_count": 2000,
                    "publish_date": "2005-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "1",
                "_score": 1,
                "_source": {
                    "author": "calvin",
                    "title": "太极拳",
                    "word_count": 1000,
                    "publish_date": "2000-10-01"
                }
            }
        ]
    }
}

（3）查询出版日期在某个范围（2017-01-01至2017-12-31）的书籍

{
    "query":{
        "range":{
            "publish_date":{
                "gte":"2015-01-01",
                "lte":"2018-12-31"
            }
        }
    }
}

返回数据

{
    "took": 8,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 1,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "10",
                "_score": 1,
                "_source": {
                    "author": "周文",
                    "title": "ElasticSearch实战",
                    "word_count": 10000,
                    "publish_date": "2015-09-28"
                }
            }
        ]
    }
}
```

###### 2、filter

```
（1）查询字数1000的书籍

{
    "query":{
        "bool":{
            "filter":{
                "term":{
                    "word_count":1000
                }
            }
        }
    }
}

返回数据

{
    "took": 10,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 0,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "4",
                "_score": 0,
                "_source": {
                    "author": "王五",
                    "title": "ElasticSearch入门",
                    "word_count": 1000,
                    "publish_date": "2010-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "1",
                "_score": 0,
                "_source": {
                    "author": "calvin",
                    "title": "太极拳",
                    "word_count": 1000,
                    "publish_date": "2000-10-01"
                }
            }
        ]
    }
}
```

###### 3、复合条件查询：以一定的逻辑组合子条件查询

```
（1）全文搜索-标题含有ElasticSearch的书籍

{
    "query":{
        "constant_score":{
            "filter":{
                "match":{
                    "title": "ElasticSearch"
                }
            },
            "boost":2
        }
    }
}

返回数据

{
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 2,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "4",
                "_score": 2,
                "_source": {
                    "author": "王五",
                    "title": "ElasticSearch入门",
                    "word_count": 1000,
                    "publish_date": "2010-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "10",
                "_score": 2,
                "_source": {
                    "author": "周文",
                    "title": "ElasticSearch实战",
                    "word_count": 10000,
                    "publish_date": "2015-09-28"
                }
            }
        ]
    }
}

（2）布尔查询-should满足任意条件

{
    "query":{
        "bool":{
            "should":[
                {
                    "match":{
                        "author":"calvin"
                    }
                },
                {
                    "match":{
                        "title":"霸道总裁"
                    }
                }
            ]
        }
    }
}

返回数据

{
    "took": 5,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 2,
        "max_score": 1.4599355,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "1",
                "_score": 1.4599355,
                "_source": {
                    "author": "calvin",
                    "title": "太极拳",
                    "word_count": 1000,
                    "publish_date": "2000-10-01"
                }
            },
            {
                "_index": "book",
                "_type": "novel",
                "_id": "8",
                "_score": 1.1507283,
                "_source": {
                    "author": "孙月",
                    "title": "霸道总裁",
                    "word_count": 7600,
                    "publish_date": "2010-10-01"
                }
            }
        ]
    }
}

（3）布尔查询-must满足全部条件

{
    "query":{
        "bool":{
            "must":[
                {
                    "match":{
                        "author":"周文"
                    }
                },
                {
                    "match":{
                        "title":"ElasticSearch"
                    }
                }
            ]
        }
    }
}

返回数据

{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 3.586249,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "10",
                "_score": 3.586249,
                "_source": {
                    "author": "周文",
                    "title": "ElasticSearch实战",
                    "word_count": 10000,
                    "publish_date": "2015-09-28"
                }
            }
        ]
    }
}

（4）使用must和filter复合查询

{
    "query":{
        "bool":{
            "must":[
                {
                    "match":{
                        "author":"周文"
                    }
                },
                {
                    "match":{
                        "title":"ElasticSearch"
                    }
                }
            ],
            "filter":[
                {
                    "term":{
                        "word_count":10000
                    }
                }
            ]
        }
    }
}

返回数据

{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 3,
        "successful": 3,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": 1,
        "max_score": 3.586249,
        "hits": [
            {
                "_index": "book",
                "_type": "novel",
                "_id": "10",
                "_score": 3.586249,
                "_source": {
                    "author": "周文",
                    "title": "ElasticSearch实战",
                    "word_count": 10000,
                    "publish_date": "2015-09-28"
                }
            }
        ]
    }
}
```

## 五、SpringBoot集成ES

pom.xml引入elasticSearch.transport

```
<dependency>
	<groupId>org.elasticsearch.client</groupId>
	<artifactId>transport</artifactId>
	<version>5.6.1</version>
</dependency>
```

创建ElasticSearch配置类

```
package com.calvin.es.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Title EsConfig
 * @Description ElasticSearch配置类
 * @author calvin
 * @date: 2019/5/27 1:23 PM 
 */

@Configuration
public class EsConfig {

    @Bean
    public TransportClient client() throws UnknownHostException {
        InetSocketTransportAddress node = new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300);
        Settings settings = Settings.builder()
                // es集群名称
                .put("cluster.name", "calvin")
                .build();
        TransportClient client = new PreBuiltTransportClient(settings);
        client.addTransportAddress(node);
        return client;
    }
}
```

#### 查询单个文档

```
/**
 * 查询单个文档
 * @param id 文档id
 * @return
 */
@GetMapping(value = "get/book/novel")
public ResponseEntity<?> getAll(@RequestParam(name = "id", defaultValue = "") String id) {
    if (id.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    GetResponse result = this.client.prepareGet("book", "novel", id).get();
    if (!result.isExists()) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(result.getSource(), HttpStatus.OK);
}
```
![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155310_9f451f04_880818.jpeg "15589640860804.jpg")

通过elasticsearch-head查看，发现id=8的文档正是我们程序查询到的结果

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155419_c895ae20_880818.jpeg "15589715183562.jpg")

#### 增加单个文档

```
/**
 * 增加单个文档
 * @param book Book封装类
 * @return
 */
@PostMapping("/add/book/novel")
public ResponseEntity<?> add(@RequestBody Book book) {
    try {
        XContentBuilder content = XContentFactory.jsonBuilder()
                .startObject()
                .field("title", book.getTitle())
                .field("author", book.getAuthor())
                .field("word_count", book.getWordCount())
                .field("publish_date", DateUtils.formatDate(book.getPublishDate(), "yyyy-MM-dd"))
                .endObject();
        IndexResponse result = this.client.prepareIndex("book", "novel").setSource(content).get();
        return new ResponseEntity<>(result.getId(), HttpStatus.OK);
    } catch (IOException e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
```

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155538_bc888fcd_880818.jpeg "15589705072698.jpg")

刷新elasticsearch-head，可以看到新创建出来的文档

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155641_a904fb39_880818.jpeg "15589705366987.jpg")

#### 修改单个文档

``` 
/**
 * 修改单个文档
 * @param book Book封装类
 * @param id 文档id
 * @return
 */
@PutMapping(value = "/update/book/novel/{id}")
public ResponseEntity<?> update(@RequestBody Book book, @PathVariable(value = "id") String id) {
    UpdateRequest update = new UpdateRequest("book", "novel", id);
    try {
        XContentBuilder builder = XContentFactory.jsonBuilder()
                .startObject();
        if (!StringUtils.isEmpty(book.getTitle())) {
            builder.field("title", book.getTitle());
        }
        if (!StringUtils.isEmpty(book.getTitle())) {
            builder.field("author", book.getAuthor());
        }
        builder.endObject();
        update.doc(builder);
        UpdateResponse result = this.client.update(update).get();
        return new ResponseEntity<>(result.toString(), HttpStatus.OK);
    } catch (Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
```

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155727_c53a4e25_880818.jpeg "15589707378643.jpg")

刷新elasticsearch-head，发现之前创建的文档title和author已经修改

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155756_10c2502a_880818.jpeg "15589707614813.jpg")

#### 删除单个文档

```
/**
 * 删除单个文档
 * @param id 文档id
 * @return
 */
@DeleteMapping(value = "/delete/book/novel/{id}")
public ResponseEntity<?> delete(@PathVariable(value = "id") String id) {
    DeleteResponse result = this.client.prepareDelete("book", "novel", id).get();
    return new ResponseEntity<>(result.toString(), HttpStatus.OK);
}
```

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155833_530c1233_880818.jpeg "15589708827979.jpg")

刷新elasticsearch-head，发现之前创建的文档已经删除

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155858_298a328d_880818.jpeg "15589709287896.jpg")

#### 符合查询
筛选title中包含“入门”，并且字数大于2000，少于20000的文档

```
/**
 * 复合查询
 * @param title 标题
 * @param gtWordCount 字数大于
 * @param ltWordCount 字数少于
 * @return
 */
@GetMapping("/query/book/novel")
public ResponseEntity<?> query(
        @RequestParam(name = "title", required = false) String title,
        @RequestParam(name = "gt_word_count", defaultValue = "0") int gtWordCount,
        @RequestParam(name = "lt_word_count", required = false) Integer ltWordCount) {
    // 构建布尔查询
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    if (!StringUtils.isEmpty(title)) {
        boolQuery.must(QueryBuilders.matchQuery("title", title));
    }
    // 构建范围查询
    RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("word_count").from(gtWordCount);
    if (ltWordCount != null && ltWordCount > 0) {
        rangeQuery.to(ltWordCount);
    }
    // 使用filter构建
    boolQuery.filter(rangeQuery);
    SearchRequestBuilder builder = this.client.prepareSearch("book")
            .setTypes("novel")
            .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
            .setQuery(boolQuery)
            .setFrom(0)
            .setSize(10);
    log.info("[ES查询请求参数]：" + builder);
    SearchResponse response = builder.get();
    List<Map<String, Object>> result = new ArrayList<>();
    for (SearchHit hit : response.getHits()) {
        result.add(hit.getSource());
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
}
```    

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155931_41713f8e_880818.jpeg "15589709856841.jpg")

验证下程序筛选出来的结果，发现没有问题

![输入图片说明](https://images.gitee.com/uploads/images/2019/1024/155953_048de085_880818.jpeg "15589711106943.jpg")















