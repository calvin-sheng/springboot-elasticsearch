package com.calvin.es.entity;

import java.util.Date;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Title Book
 * @Description Book封装类
 * @author calvin
 * @date: 2019/5/27 2:31 PM 
 */

@Data
public class Book {

    private String id;

    private String title;

    private String author;

    private int wordCount;

    private Date publishDate;

}
