package com.calvin.es.controller;

import com.calvin.es.entity.Book;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.DateUtils;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title BookController
 * @Description es的增删改查
 * @author calvin
 * @date: 2019/5/27 1:25 PM 
 */

@RestController
@Slf4j
public class BookController {

    @Autowired
    private TransportClient client;

    /**
     * 查询单个文档
     * @param id 文档id
     * @return
     */
    @GetMapping(value = "get/book/novel")
    public ResponseEntity<?> getAll(@RequestParam(name = "id", defaultValue = "") String id) {
        if (id.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        GetResponse result = this.client.prepareGet("book", "novel", id).get();
        if (!result.isExists()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(result.getSource(), HttpStatus.OK);
    }

    /**
     * 增加单个文档
     * @param book Book封装类
     * @return
     */
    @PostMapping("/add/book/novel")
    public ResponseEntity<?> add(@RequestBody Book book) {
        try {
            XContentBuilder content = XContentFactory.jsonBuilder()
                    .startObject()
                    .field("title", book.getTitle())
                    .field("author", book.getAuthor())
                    .field("word_count", book.getWordCount())
                    .field("publish_date", DateUtils.formatDate(book.getPublishDate(), "yyyy-MM-dd"))
                    .endObject();
            IndexResponse result = this.client.prepareIndex("book", "novel").setSource(content).get();
            return new ResponseEntity<>(result.getId(), HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 修改单个文档
     * @param book Book封装类
     * @param id 文档id
     * @return
     */
    @PutMapping(value = "/update/book/novel/{id}")
    public ResponseEntity<?> update(@RequestBody Book book, @PathVariable(value = "id") String id) {
        UpdateRequest update = new UpdateRequest("book", "novel", id);
        try {
            XContentBuilder builder = XContentFactory.jsonBuilder()
                    .startObject();
            if (!StringUtils.isEmpty(book.getTitle())) {
                builder.field("title", book.getTitle());
            }
            if (!StringUtils.isEmpty(book.getTitle())) {
                builder.field("author", book.getAuthor());
            }
            builder.endObject();
            update.doc(builder);
            UpdateResponse result = this.client.update(update).get();
            return new ResponseEntity<>(result.toString(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 删除单个文档
     * @param id 文档id
     * @return
     */
    @DeleteMapping(value = "/delete/book/novel/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") String id) {
        DeleteResponse result = this.client.prepareDelete("book", "novel", id).get();
        return new ResponseEntity<>(result.toString(), HttpStatus.OK);
    }

    /**
     * 复合查询
     * @param title 标题
     * @param gtWordCount 字数大于
     * @param ltWordCount 字数少于
     * @return
     */
    @GetMapping("/query/book/novel")
    public ResponseEntity<?> query(
            @RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "gt_word_count", defaultValue = "0") int gtWordCount,
            @RequestParam(name = "lt_word_count", required = false) Integer ltWordCount) {
        // 构建布尔查询
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(title)) {
            boolQuery.must(QueryBuilders.matchQuery("title", title));
        }
        // 构建范围查询
        RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("word_count").from(gtWordCount);
        if (ltWordCount != null && ltWordCount > 0) {
            rangeQuery.to(ltWordCount);
        }
        // 使用filter构建
        boolQuery.filter(rangeQuery);
        SearchRequestBuilder builder = this.client.prepareSearch("book")
                .setTypes("novel")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(boolQuery)
                .setFrom(0)
                .setSize(10);
        log.info("[ES查询请求参数]：" + builder);
        SearchResponse response = builder.get();
        List<Map<String, Object>> result = new ArrayList<>();
        for (SearchHit hit : response.getHits()) {
            result.add(hit.getSource());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
